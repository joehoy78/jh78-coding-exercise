package com.gamesys.codingexercise.joehoy.mock;

import com.gamesys.codingexercise.joehoy.ExclusionService;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/exclusion")
public class MockExclusionService implements ExclusionService {

	public MockExclusionService() {
		// TODO Auto-generated constructor stub
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response doPost(
            			@FormParam("dob") String dob,
            			@FormParam("ssn") String ssn
            			) throws IOException {
		if (validate(dob, ssn)) {
			return Response.ok("{\"exclusionPass\":\"true\"}", MediaType.APPLICATION_JSON).build();
		} else {
			return Response.ok("{\"exclusionPass\":\"false\"}", MediaType.APPLICATION_JSON).build();
		}
	}
	
	@Override
	public boolean validate(String dob, String ssn) {
		// Mock condition to simulate blacklist rejection
		if (dob.equals("1988-10-20") && ssn.equals("123-45-6789")) {
			return false;
		}
		return true;
	}
}
