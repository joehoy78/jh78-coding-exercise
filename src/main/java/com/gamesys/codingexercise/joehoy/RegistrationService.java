package com.gamesys.codingexercise.joehoy;

import java.io.IOException;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;


@Path("/register")
public class RegistrationService {
	
	private HashMap<String, RegistrationEntry> registeredUsers;
	
	public RegistrationService() {
		registeredUsers = new HashMap<String, RegistrationEntry>();
	}
	
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response doPost(	@FormParam("username") String username,
            			@FormParam("password") String password,
            			@FormParam("dob") String dob,
            			@FormParam("ssn") String ssn
            			) throws IOException {
        try {        	
        	RegistrationEntry newEntry = 
        			validateAndBuildEntry(username, password, dob, ssn);
        } catch (InvalidEntryException e) {
        	return Response.serverError().entity("{\"error\" : \"Invalid entry field\"}").build();
        }
        // JH - Yes, this check is ugly - however I was running short on time
        // and the myriad version conflicts regarding Jersey/JSWS JSON parsing 
        // were somewhat of a headache.
        // As I understand it, implementation of this part is not being assessed
        // in any case.
        Response blacklistResponse = validateAgainstBlacklist(dob, ssn);
        String resString = blacklistResponse.readEntity(String.class);
        if (resString.contains("false")) {
        	return Response.serverError().entity("{\"error\" : \"User is blacklisted\"}").build();
        }
        return Response.ok("{\"entryStored\":\"true\"}", MediaType.APPLICATION_JSON).build();
    }
        
    public Response validateAgainstBlacklist(String dob, String ssn) {
    	Response response;
    	Client client = ClientBuilder.newClient();
    	Form form = new Form()
		    			.param("dob", dob)
		                .param("ssn", ssn);
		                response = client.target("http://localhost:8080/exclusion/")
		                .request()
		                .post(Entity.form(form));
		return response;
    }
	
	public void storeRegistrationEntry(RegistrationEntry entry) {
		registeredUsers.put(entry.getSsn(), entry);
	}
	
	public HashMap<String, RegistrationEntry> getRegisteredUsers() {
		return registeredUsers;
	}
	
	public RegistrationEntry validateAndBuildEntry(String username, String password, String dob, String ssn) throws InvalidEntryException {
		if (validateUsername(username) &&
			validatePassword(password) &&
			validateDob(dob)) {
			return new RegistrationEntry(username, password, dob, ssn);
		} else {
			throw new InvalidEntryException();
		}
	}
	
	boolean validateUsername(String username) {
		if (null != username) {
			if (username.matches("[a-zA-Z0-9]+")) {				
				return true;
			}
		}
		return false;
	}
	
	boolean validatePassword(String password) {
		if (null != password) {
			if (password.matches("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{4,}$")) {				
				return true;
			}
		}
		return false;
	}
	
	boolean validateDob(String dob) {
		if (null != dob) {
			if (dob.matches("^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])$")) {
				return true;
			}
		}
		return false;
	}
}
