package com.gamesys.codingexercise.joehoy;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class RegistrationServiceTest {
	
	private RegistrationService registrationService;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		registrationService = new RegistrationService();
	}

	@After
	public void tearDown() throws Exception {
		registrationService = null;
	}

	@Test
	public void testUsernameFailsValidationWithNull() {
		assertFalse(registrationService.validateUsername(null));
	}
	
	@Test
	public void testUsernameFailsValidationWithEmptyString() {
		assertFalse(registrationService.validateUsername(""));
	}
	
	@Test
	public void testUsernameFailsValidationWithNonAlphanumericCharacter() {
		assertFalse(registrationService.validateUsername("testusername$"));
	}
	
	@Test
	public void testUsernameFailsValidationWithSpace() {
		assertFalse(registrationService.validateUsername("test username"));
	}
	
	@Test
	public void testUsernameFailsValidationWithSpaceAndNonAlphaNumericCharacters() {
		assertFalse(registrationService.validateUsername("test username£$%"));
	}
	
	@Test
	public void testUsernamePassesValidationWithValidUsername() {
		assertTrue(registrationService.validateUsername("testusername"));
	}
	
	@Test
	public void testPasswordFailsValidationWithNull() {
		assertFalse(registrationService.validatePassword(null));
	}
	
	@Test
	public void testPasswordFailsValidationWithEmptyString() {
		assertFalse(registrationService.validatePassword(""));
	}
	
	@Test
	public void testPasswordFailsValidationWithLessThan4Chars() {
		assertFalse(registrationService.validatePassword("A1"));
		assertFalse(registrationService.validatePassword("A1B"));
	}
	
	@Test
	public void testPasswordFailsValidationWithNoUpperCaseChar() {
		assertFalse(registrationService.validatePassword("noupperc4se"));
	}
	
	@Test
	public void testPasswordFailsValidationWithNoNumericChar() {
		assertFalse(registrationService.validatePassword("noNumeric"));
	}
	
	@Test
	public void testPasswordPassesValidationWith4charValidUsername() {
		assertTrue(registrationService.validatePassword("T3st"));
	}
	
	@Test
	public void testPasswordPassesValidationWithValidUsername() {
		assertTrue(registrationService.validatePassword("testP4ssword"));
	}
	
	@Test
	public void testDobFailsValidationWithNull() {
		assertFalse(registrationService.validateDob(null));
	}
	
	@Test
	public void testDobFailsValidationWithEmptyString() {
		assertFalse(registrationService.validateDob(""));
	}
	
	@Test
	public void testDobFailsValidationWithAlphaChars() {
		assertFalse(registrationService.validateDob("ABCDEFG"));
	}
	
	@Test
	public void testDobFailsValidationWithBadFormat() {
		assertFalse(registrationService.validateDob("18/11/2018"));
	}
	
	@Test
	public void testDobPassesValidationWithValidDateOfBirth() {
		assertTrue(registrationService.validateDob("2018-11-18"));
	}
	
	@Test
	public void testEntryIsStored() {
		RegistrationEntry testEntry = new RegistrationEntry("testusername", 
															"testP4ssword", 
															"1988-10-20", 
															"123456");
		registrationService.storeRegistrationEntry(testEntry);
		assertNotNull(registrationService.getRegisteredUsers().get("123456"));
	}
	
	@Test(expected = InvalidEntryException.class)
	public void testEntryValidationFailsWithInvalidUsername() {
		RegistrationEntry entry =
		registrationService.validateAndBuildEntry(	"test username", 
													"testP4ssword", 
													"1988-10-20", 
													"123456");
	}
	
	@Test(expected = InvalidEntryException.class)
	public void testEntryValidationFailsWithInvalidPassword() {
		RegistrationEntry entry =
		registrationService.validateAndBuildEntry(	"testusername", 
													"testpassword", 
													"1988-10-20", 
													"123456");
	}
	
	@Test(expected = InvalidEntryException.class)
	public void testEntryValidationFailsWithInvalidDob() {
		RegistrationEntry entry =
		registrationService.validateAndBuildEntry(	"testusername", 
													"testP4ssword", 
													"19881020", 
													"123456");
	}
	
	@Test
	public void testEntryValidationPassesWithValidEntry() {
		RegistrationEntry entry =
		registrationService.validateAndBuildEntry(	"testusername", 
													"testP4ssword", 
													"1988-10-20", 
													"123456");
	}
}
